﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ProjectPiggyAPI.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccountType",
                columns: table => new
                {
                    AccountTypeID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccountTypeTitle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountType", x => x.AccountTypeID);
                });

            migrationBuilder.CreateTable(
                name: "Answers",
                columns: table => new
                {
                    AnswersID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Answer = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Answers", x => x.AnswersID);
                });

            migrationBuilder.CreateTable(
                name: "Organisation",
                columns: table => new
                {
                    OrganisationID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    UniqueKey = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Organisation", x => x.OrganisationID);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    ProductID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DelFlag = table.Column<bool>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: false),
                    ProductDescription = table.Column<string>(nullable: true),
                    ProductTitle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.ProductID);
                });

            migrationBuilder.CreateTable(
                name: "TestSheet",
                columns: table => new
                {
                    TestSheetID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: false),
                    TestSheetDescription = table.Column<string>(nullable: true),
                    TestSheetTitle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestSheet", x => x.TestSheetID);
                });

            migrationBuilder.CreateTable(
                name: "Types",
                columns: table => new
                {
                    TypesID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TypesTitle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Types", x => x.TypesID);
                });

            migrationBuilder.CreateTable(
                name: "Account",
                columns: table => new
                {
                    AccountID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccountTypeID = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DelFlag = table.Column<bool>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    OrganisationID = table.Column<int>(nullable: false),
                    Password = table.Column<string>(nullable: true),
                    Salt = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Account", x => x.AccountID);
                    table.ForeignKey(
                        name: "FK_Account_AccountType_AccountTypeID",
                        column: x => x.AccountTypeID,
                        principalTable: "AccountType",
                        principalColumn: "AccountTypeID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Account_Organisation_OrganisationID",
                        column: x => x.OrganisationID,
                        principalTable: "Organisation",
                        principalColumn: "OrganisationID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductItems",
                columns: table => new
                {
                    ProductItemsID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DelFlag = table.Column<bool>(nullable: false),
                    LastUpdated = table.Column<DateTime>(nullable: false),
                    ProductID = table.Column<int>(nullable: false),
                    ProductItemDescription = table.Column<string>(nullable: true),
                    ProductTitle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductItems", x => x.ProductItemsID);
                    table.ForeignKey(
                        name: "FK_ProductItems_Product_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Product",
                        principalColumn: "ProductID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QuestionValidation",
                columns: table => new
                {
                    QuestionValidationID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Placeholder = table.Column<string>(nullable: true),
                    Regex = table.Column<string>(nullable: true),
                    TypeID = table.Column<int>(nullable: false),
                    TypesID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestionValidation", x => x.QuestionValidationID);
                    table.ForeignKey(
                        name: "FK_QuestionValidation_Types_TypesID",
                        column: x => x.TypesID,
                        principalTable: "Types",
                        principalColumn: "TypesID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountToProducts",
                columns: table => new
                {
                    AccountsToProductsID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccountID = table.Column<int>(nullable: false),
                    ProductID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountToProducts", x => x.AccountsToProductsID);
                    table.ForeignKey(
                        name: "FK_AccountToProducts_Account_AccountID",
                        column: x => x.AccountID,
                        principalTable: "Account",
                        principalColumn: "AccountID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountToProducts_Product_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Product",
                        principalColumn: "ProductID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductItemToTest",
                columns: table => new
                {
                    ProductItemToTestID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductItemID = table.Column<int>(nullable: false),
                    ProductItemsID = table.Column<int>(nullable: true),
                    TestSheetID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductItemToTest", x => x.ProductItemToTestID);
                    table.ForeignKey(
                        name: "FK_ProductItemToTest_ProductItems_ProductItemsID",
                        column: x => x.ProductItemsID,
                        principalTable: "ProductItems",
                        principalColumn: "ProductItemsID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductItemToTest_TestSheet_TestSheetID",
                        column: x => x.TestSheetID,
                        principalTable: "TestSheet",
                        principalColumn: "TestSheetID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Questions",
                columns: table => new
                {
                    QuestionsID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnswerID = table.Column<int>(nullable: false),
                    AnswersID = table.Column<int>(nullable: true),
                    MyProperty = table.Column<int>(nullable: false),
                    OrderNum = table.Column<int>(nullable: false),
                    Question = table.Column<string>(nullable: true),
                    QuestionValidationID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questions", x => x.QuestionsID);
                    table.ForeignKey(
                        name: "FK_Questions_Answers_AnswersID",
                        column: x => x.AnswersID,
                        principalTable: "Answers",
                        principalColumn: "AnswersID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Questions_QuestionValidation_QuestionValidationID",
                        column: x => x.QuestionValidationID,
                        principalTable: "QuestionValidation",
                        principalColumn: "QuestionValidationID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QuestionToTestSheet",
                columns: table => new
                {
                    QuestionToTestSheetID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuestionID = table.Column<int>(nullable: false),
                    QuestionsID = table.Column<int>(nullable: true),
                    TestSheetID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestionToTestSheet", x => x.QuestionToTestSheetID);
                    table.ForeignKey(
                        name: "FK_QuestionToTestSheet_Questions_QuestionsID",
                        column: x => x.QuestionsID,
                        principalTable: "Questions",
                        principalColumn: "QuestionsID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_QuestionToTestSheet_TestSheet_TestSheetID",
                        column: x => x.TestSheetID,
                        principalTable: "TestSheet",
                        principalColumn: "TestSheetID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Values",
                columns: table => new
                {
                    ValuesID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuestionID = table.Column<int>(nullable: false),
                    QuestionsID = table.Column<int>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Values", x => x.ValuesID);
                    table.ForeignKey(
                        name: "FK_Values_Questions_QuestionsID",
                        column: x => x.QuestionsID,
                        principalTable: "Questions",
                        principalColumn: "QuestionsID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Account_AccountTypeID",
                table: "Account",
                column: "AccountTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Account_OrganisationID",
                table: "Account",
                column: "OrganisationID");

            migrationBuilder.CreateIndex(
                name: "IX_AccountToProducts_AccountID",
                table: "AccountToProducts",
                column: "AccountID");

            migrationBuilder.CreateIndex(
                name: "IX_AccountToProducts_ProductID",
                table: "AccountToProducts",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductItems_ProductID",
                table: "ProductItems",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductItemToTest_ProductItemsID",
                table: "ProductItemToTest",
                column: "ProductItemsID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductItemToTest_TestSheetID",
                table: "ProductItemToTest",
                column: "TestSheetID");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_AnswersID",
                table: "Questions",
                column: "AnswersID");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_QuestionValidationID",
                table: "Questions",
                column: "QuestionValidationID");

            migrationBuilder.CreateIndex(
                name: "IX_QuestionToTestSheet_QuestionsID",
                table: "QuestionToTestSheet",
                column: "QuestionsID");

            migrationBuilder.CreateIndex(
                name: "IX_QuestionToTestSheet_TestSheetID",
                table: "QuestionToTestSheet",
                column: "TestSheetID");

            migrationBuilder.CreateIndex(
                name: "IX_QuestionValidation_TypesID",
                table: "QuestionValidation",
                column: "TypesID");

            migrationBuilder.CreateIndex(
                name: "IX_Values_QuestionsID",
                table: "Values",
                column: "QuestionsID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountToProducts");

            migrationBuilder.DropTable(
                name: "ProductItemToTest");

            migrationBuilder.DropTable(
                name: "QuestionToTestSheet");

            migrationBuilder.DropTable(
                name: "Values");

            migrationBuilder.DropTable(
                name: "Account");

            migrationBuilder.DropTable(
                name: "ProductItems");

            migrationBuilder.DropTable(
                name: "TestSheet");

            migrationBuilder.DropTable(
                name: "Questions");

            migrationBuilder.DropTable(
                name: "AccountType");

            migrationBuilder.DropTable(
                name: "Organisation");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "Answers");

            migrationBuilder.DropTable(
                name: "QuestionValidation");

            migrationBuilder.DropTable(
                name: "Types");
        }
    }
}
