﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectPiggyAPI.Migrations
{
    [DbContext(typeof(ProjectPiggyContext))]
    [Migration("20170706194051_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ProjectPiggyAPI.Models.Account", b =>
                {
                    b.Property<int>("AccountID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccountTypeID");

                    b.Property<DateTime>("DateCreated");

                    b.Property<bool>("DelFlag");

                    b.Property<string>("Email");

                    b.Property<int>("OrganisationID");

                    b.Property<string>("Password");

                    b.Property<string>("Salt");

                    b.Property<string>("Username");

                    b.HasKey("AccountID");

                    b.HasIndex("AccountTypeID");

                    b.HasIndex("OrganisationID");

                    b.ToTable("Account");
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.AccountsToProducts", b =>
                {
                    b.Property<int>("AccountsToProductsID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccountID");

                    b.Property<int>("ProductID");

                    b.HasKey("AccountsToProductsID");

                    b.HasIndex("AccountID");

                    b.HasIndex("ProductID");

                    b.ToTable("AccountToProducts");
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.AccountType", b =>
                {
                    b.Property<int>("AccountTypeID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AccountTypeTitle");

                    b.HasKey("AccountTypeID");

                    b.ToTable("AccountType");
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.Answers", b =>
                {
                    b.Property<int>("AnswersID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Answer");

                    b.HasKey("AnswersID");

                    b.ToTable("Answers");
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.Organisation", b =>
                {
                    b.Property<int>("OrganisationID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateCreated");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<string>("UniqueKey");

                    b.HasKey("OrganisationID");

                    b.ToTable("Organisation");
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.Product", b =>
                {
                    b.Property<int>("ProductID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateCreated");

                    b.Property<bool>("DelFlag");

                    b.Property<DateTime>("LastUpdated");

                    b.Property<string>("ProductDescription");

                    b.Property<string>("ProductTitle");

                    b.HasKey("ProductID");

                    b.ToTable("Product");
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.ProductItems", b =>
                {
                    b.Property<int>("ProductItemsID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateCreated");

                    b.Property<bool>("DelFlag");

                    b.Property<DateTime>("LastUpdated");

                    b.Property<int>("ProductID");

                    b.Property<string>("ProductItemDescription");

                    b.Property<string>("ProductTitle");

                    b.HasKey("ProductItemsID");

                    b.HasIndex("ProductID");

                    b.ToTable("ProductItems");
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.ProductItemToTest", b =>
                {
                    b.Property<int>("ProductItemToTestID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ProductItemID");

                    b.Property<int?>("ProductItemsID");

                    b.Property<int>("TestSheetID");

                    b.HasKey("ProductItemToTestID");

                    b.HasIndex("ProductItemsID");

                    b.HasIndex("TestSheetID");

                    b.ToTable("ProductItemToTest");
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.Questions", b =>
                {
                    b.Property<int>("QuestionsID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AnswerID");

                    b.Property<int?>("AnswersID");

                    b.Property<int>("MyProperty");

                    b.Property<int>("OrderNum");

                    b.Property<string>("Question");

                    b.Property<int>("QuestionValidationID");

                    b.HasKey("QuestionsID");

                    b.HasIndex("AnswersID");

                    b.HasIndex("QuestionValidationID");

                    b.ToTable("Questions");
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.QuestionToTestSheet", b =>
                {
                    b.Property<int>("QuestionToTestSheetID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("QuestionID");

                    b.Property<int?>("QuestionsID");

                    b.Property<int>("TestSheetID");

                    b.HasKey("QuestionToTestSheetID");

                    b.HasIndex("QuestionsID");

                    b.HasIndex("TestSheetID");

                    b.ToTable("QuestionToTestSheet");
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.QuestionValidation", b =>
                {
                    b.Property<int>("QuestionValidationID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Placeholder");

                    b.Property<string>("Regex");

                    b.Property<int>("TypeID");

                    b.Property<int?>("TypesID");

                    b.HasKey("QuestionValidationID");

                    b.HasIndex("TypesID");

                    b.ToTable("QuestionValidation");
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.TestSheet", b =>
                {
                    b.Property<int>("TestSheetID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime>("LastUpdated");

                    b.Property<string>("TestSheetDescription");

                    b.Property<string>("TestSheetTitle");

                    b.HasKey("TestSheetID");

                    b.ToTable("TestSheet");
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.Types", b =>
                {
                    b.Property<int>("TypesID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("TypesTitle");

                    b.HasKey("TypesID");

                    b.ToTable("Types");
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.Values", b =>
                {
                    b.Property<int>("ValuesID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("QuestionID");

                    b.Property<int?>("QuestionsID");

                    b.Property<string>("Value");

                    b.HasKey("ValuesID");

                    b.HasIndex("QuestionsID");

                    b.ToTable("Values");
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.Account", b =>
                {
                    b.HasOne("ProjectPiggyAPI.Models.AccountType", "AccountType")
                        .WithMany()
                        .HasForeignKey("AccountTypeID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ProjectPiggyAPI.Models.Organisation", "Organisation")
                        .WithMany("Posts")
                        .HasForeignKey("OrganisationID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.AccountsToProducts", b =>
                {
                    b.HasOne("ProjectPiggyAPI.Models.Account", "Account")
                        .WithMany("AccountToProducts")
                        .HasForeignKey("AccountID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ProjectPiggyAPI.Models.Product", "Product")
                        .WithMany("AccountToProduct")
                        .HasForeignKey("ProductID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.ProductItems", b =>
                {
                    b.HasOne("ProjectPiggyAPI.Models.Product", "Product")
                        .WithMany("ProductItems")
                        .HasForeignKey("ProductID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.ProductItemToTest", b =>
                {
                    b.HasOne("ProjectPiggyAPI.Models.ProductItems", "ProductItems")
                        .WithMany("ProductItemToTest")
                        .HasForeignKey("ProductItemsID");

                    b.HasOne("ProjectPiggyAPI.Models.TestSheet", "TestSheet")
                        .WithMany("ProductItemToTest")
                        .HasForeignKey("TestSheetID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.Questions", b =>
                {
                    b.HasOne("ProjectPiggyAPI.Models.Answers", "Answers")
                        .WithMany("Questions")
                        .HasForeignKey("AnswersID");

                    b.HasOne("ProjectPiggyAPI.Models.QuestionValidation", "QuestionValidation")
                        .WithMany()
                        .HasForeignKey("QuestionValidationID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.QuestionToTestSheet", b =>
                {
                    b.HasOne("ProjectPiggyAPI.Models.Questions", "Questions")
                        .WithMany()
                        .HasForeignKey("QuestionsID");

                    b.HasOne("ProjectPiggyAPI.Models.TestSheet", "TestSheet")
                        .WithMany("QuestionToTestSheet")
                        .HasForeignKey("TestSheetID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.QuestionValidation", b =>
                {
                    b.HasOne("ProjectPiggyAPI.Models.Types", "Types")
                        .WithMany("QuestionValidation")
                        .HasForeignKey("TypesID");
                });

            modelBuilder.Entity("ProjectPiggyAPI.Models.Values", b =>
                {
                    b.HasOne("ProjectPiggyAPI.Models.Questions", "Questions")
                        .WithMany("Values")
                        .HasForeignKey("QuestionsID");
                });
        }
    }
}
