﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public interface IFullDataAccess<T> : ISearch<T>, IAddUpdate<T>, IDelete
    {
    }
}
