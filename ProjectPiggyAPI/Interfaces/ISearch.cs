﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public interface ISearch<T>
    {
        T Get(int id);
        T Get(string name);
        IEnumerable<T> List();
        IEnumerable<T> List(string name);
    }
}
