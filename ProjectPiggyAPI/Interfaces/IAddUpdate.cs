﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public interface IAddUpdate<T>
    {
        void Add(T obj);
        void Update(T obj);
    }
}
