﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public interface IAccount<T>
    {
        bool Login(T obj);
        void Register(T obj);
        T GetByID(int id);
        void Logout();
    }
}
