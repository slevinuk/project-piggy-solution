﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public class Types
    {
        public int TypesID { get; set; }
        public string TypesTitle { get; set; }
        public List<QuestionValidation> QuestionValidation { get; set; }
    }
}
