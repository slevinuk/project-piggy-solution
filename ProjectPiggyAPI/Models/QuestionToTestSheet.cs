﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public class QuestionToTestSheet
    {
        public int QuestionToTestSheetID { get; set; }
        public int QuestionID { get; set; }
        public Questions Questions { get; set; }
        public int TestSheetID { get; set; }
        public TestSheet TestSheet { get; set; }
    }
}
