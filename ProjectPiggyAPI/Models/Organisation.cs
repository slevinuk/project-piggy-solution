﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public class Organisation
    {
        public int OrganisationID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UniqueKey { get; set; }
        public DateTime DateCreated { get; set; }

        public List<Account> Posts { get; set; }
    }
}
