﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public class Answers
    {
        public int AnswersID { get; set; }
        public string Answer { get; set; }
        public List<Questions> Questions { get; set; }
    }
}
