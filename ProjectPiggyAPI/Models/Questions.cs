﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public class Questions
    {
        public int QuestionsID { get; set; }
        public string Question { get; set; }
        public int QuestionValidationID { get; set; }
        public QuestionValidation QuestionValidation { get; set; }
        public int AnswerID { get; set; }
        public Answers Answers { get; set; }
        public int OrderNum { get; set; }
        public int MyProperty { get; set; }
        public List<Values> Values { get; set; }
    }
}
