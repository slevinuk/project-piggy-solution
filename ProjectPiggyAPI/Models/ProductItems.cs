﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public class ProductItems
    {
        public int ProductItemsID { get; set; }
        public string ProductTitle { get; set; }
        public string ProductItemDescription { get; set; }
        public bool DelFlag { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime LastUpdated { get; set; }

        public int ProductID { get; set; }
        public Product Product { get; set; }

        public List<ProductItemToTest> ProductItemToTest { get; set; }
    }
}
