﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public class TestSheet
    {
        public int TestSheetID { get; set; }
        public string TestSheetTitle { get; set; }
        public string TestSheetDescription { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public List<ProductItemToTest> ProductItemToTest { get; set; }
        public List<QuestionToTestSheet> QuestionToTestSheet { get; set; }
    }
}
