﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public class Product
    {
        public int ProductID { get; set; }
        public string ProductTitle { get; set; }
        public string ProductDescription { get; set; }
        public bool DelFlag { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime LastUpdated { get; set; }

        public List<AccountsToProducts> AccountToProduct { get; set; }
        public List<ProductItems> ProductItems { get; set; }
    }
}
