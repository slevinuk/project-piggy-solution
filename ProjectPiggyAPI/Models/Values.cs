﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public class Values
    {
        public int ValuesID { get; set; }
        public int QuestionID { get; set; }
        public Questions Questions { get; set; }
        public string Value { get; set; }
    }
}
