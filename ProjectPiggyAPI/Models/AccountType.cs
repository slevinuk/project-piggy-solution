﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public class AccountType
    {
        public int AccountTypeID { get; set; }
        public string AccountTypeTitle { get; set; }
    }
}
