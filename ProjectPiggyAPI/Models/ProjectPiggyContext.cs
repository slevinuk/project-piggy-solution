﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public class ProjectPiggyContext : DbContext
    {
        public ProjectPiggyContext(DbContextOptions<ProjectPiggyContext> options)
            : base(options)
        { }

        public DbSet<Account> Account { get; set; }
        public DbSet<AccountsToProducts> AccountToProducts { get; set; }
        public DbSet<AccountType> AccountType { get; set; }
        public DbSet<Organisation> Organisation { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductItems> ProductItems { get; set; }
        public DbSet<ProductItemToTest> ProductItemToTest { get; set; }
        public DbSet<Questions> Questions { get; set; }
        public DbSet<QuestionToTestSheet> QuestionToTestSheet { get; set; }
        public DbSet<QuestionValidation> QuestionValidation { get; set; }
        public DbSet<TestSheet> TestSheet { get; set; }
        public DbSet<Types> Types { get; set; }
        public DbSet<Values> Values { get; set; }
    }
}