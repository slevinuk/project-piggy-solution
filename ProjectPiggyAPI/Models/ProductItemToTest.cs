﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public class ProductItemToTest
    {
        public int ProductItemToTestID { get; set; }
        public int ProductItemID { get; set; }
        public ProductItems ProductItems { get; set; }
        public int TestSheetID { get; set; }
        public TestSheet TestSheet { get; set; }
    }
}
