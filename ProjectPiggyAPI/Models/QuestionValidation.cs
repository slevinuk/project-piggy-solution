﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public class QuestionValidation
    {
        public int QuestionValidationID { get; set; }
        public int TypeID { get; set; }
        public Types Types { get; set; }
        public string Placeholder { get; set; }
        public string Regex { get; set; }
    }
}
