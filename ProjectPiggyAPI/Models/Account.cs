﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public class Account
    {
        public int AccountID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string Email { get; set; }
        public bool DelFlag { get; set; }
        public DateTime DateCreated { get; set; }

        public List<AccountsToProducts> AccountToProducts { get; set; }

        public int OrganisationID { get; set; }
        public Organisation Organisation { get; set; }

        public int AccountTypeID { get; set; }
        public AccountType AccountType { get; set; }
    }
}
