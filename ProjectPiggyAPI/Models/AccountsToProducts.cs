﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI
{
    public class AccountsToProducts
    {
        public int AccountsToProductsID { get; set; }

        public int AccountID { get; set; }
        public Account Account { get; set; }

        public int ProductID { get; set; }
        public Product Product { get; set; }
    }
}
