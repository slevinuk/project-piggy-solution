using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ProjectPiggyAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Account/[action]")]
    public class AccountController : Controller
    {
        private readonly IAccount<Account> _account;
        public AccountController(IAccount<Account> account)
        {
            _account = account;
        }

        [HttpPost]
        public Account GetAccount([FromBody] int id)
        {
            return _account.GetByID(id);
        }
    }
}