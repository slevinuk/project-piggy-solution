using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ProjectPiggyAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/AccountTypes/[action]")]
    public class AccountTypesController : Controller
    {
        private readonly IFullDataAccess<AccountType> _accountType;
        public AccountTypesController(IFullDataAccess<AccountType> accountType)
        {
            _accountType = accountType;
        }

        [HttpGet] 
        public IEnumerable<AccountType> ListAccountTypes()
        {
            return _accountType.List();
        }
    }
}