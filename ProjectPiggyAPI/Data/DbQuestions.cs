﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI.Data
{
    public class DbQuestions : IFullDataAccess<Questions>
    {
        private readonly ProjectPiggyContext _context;
        
        public DbQuestions(ProjectPiggyContext context)
        {
            _context = context;
        }

        public void Add(Questions obj)
        {
            _context.Questions.Add(obj);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var questions = new Questions { QuestionsID = id };
            _context.Questions.Attach(questions);
            _context.Questions.Remove(questions);
        }

        public Questions Get(int id)
        {
            return _context.Questions.Find(id);
        }

        public Questions Get(string name)
        {
            return _context.Questions.Where(x => x.Question == name).FirstOrDefault();
        }

        public IEnumerable<Questions> List()
        {
            return _context.Questions;
        }

        public IEnumerable<Questions> List(string name)
        {
            return _context.Questions.Where(x => x.Question == name);
        }

        public void Update(Questions obj)
        {
            _context.Questions.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
