﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI.Data
{
    public class DbAccountType : IFullDataAccess<AccountType>
    {
        private readonly ProjectPiggyContext _context;
        public DbAccountType(ProjectPiggyContext context)
        {
            _context = context;
        }

        public void Add(AccountType obj)
        {
            _context.AccountType.Add(obj);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var accountType = new AccountType { AccountTypeID = id };
            _context.AccountType.Attach(accountType);
            _context.AccountType.Remove(accountType);
            _context.SaveChanges();
        }

        public AccountType Get(int id)
        {
            return _context.AccountType.Where(x => x.AccountTypeID == id).FirstOrDefault();
        }

        public AccountType Get(string name)
        {
            return _context.AccountType.Where(x => x.AccountTypeTitle == name).FirstOrDefault();
        }

        public IEnumerable<AccountType> List()
        {
            return _context.AccountType;
        }

        public IEnumerable<AccountType> List(string name)
        {
            return _context.AccountType.Where(x => x.AccountTypeTitle == name);
        }

        public void Update(AccountType obj)
        {
            _context.AccountType.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
