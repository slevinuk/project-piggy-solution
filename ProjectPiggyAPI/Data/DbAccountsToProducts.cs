﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI.Data
{
    public class DbAccountsToProducts : IAddUpdate<AccountsToProducts>, IDelete
    {
        private readonly ProjectPiggyContext _context;
        public DbAccountsToProducts(ProjectPiggyContext context)
        {
            _context = context;
        }

        public void Add(AccountsToProducts obj)
        {
            _context.AccountToProducts.Add(obj);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var accountToProducts = new AccountsToProducts { AccountsToProductsID = id };
            _context.AccountToProducts.Attach(accountToProducts);
            _context.AccountToProducts.Remove(accountToProducts);
            _context.SaveChanges();
        }

        public void Update(AccountsToProducts obj)
        {
            throw new NotImplementedException();
        }
    }
}
