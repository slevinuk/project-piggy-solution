﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI.Data
{
    public class DbProductItems : IFullDataAccess<ProductItems>
    {
        private readonly ProjectPiggyContext _context;
        public DbProductItems(ProjectPiggyContext context)
        {
            _context = context;
        }

        public void Add(ProductItems obj)
        {
            _context.ProductItems.Add(obj);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public ProductItems Get(int id)
        {
            throw new NotImplementedException();
        }

        public ProductItems Get(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProductItems> List()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProductItems> List(string name)
        {
            throw new NotImplementedException();
        }

        public void Update(ProductItems obj)
        {
            throw new NotImplementedException();
        }
    }
}
