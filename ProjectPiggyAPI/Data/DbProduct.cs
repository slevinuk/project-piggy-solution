﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI.Data
{
    public class DbProduct : IFullDataAccess<Product>
    {
        private readonly ProjectPiggyContext _context;
        public DbProduct(ProjectPiggyContext context)
        {
            _context = context;
        }

        public void Add(Product obj)
        {
            _context.Product.Add(obj);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var products = new Product { ProductID = id };
            _context.Product.Attach(products);
            _context.Product.Remove(products);
            _context.SaveChanges();
        }

        public Product Get(int id)
        {
            return _context.Product.Where(x => x.ProductID == id).FirstOrDefault();
        }

        public Product Get(string name)
        {
            return _context.Product.Where(x => x.ProductTitle == name).FirstOrDefault();
        }

        public IEnumerable<Product> List()
        {
            return _context.Product;
        }

        public IEnumerable<Product> List(string name)
        {
            return _context.Product.Where(x => x.ProductTitle == name);
        }

        public void Update(Product obj)
        {
            _context.Product.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
