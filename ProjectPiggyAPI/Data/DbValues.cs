﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI.Data
{
    public class DbValues : IFullDataAccess<Values>
    {
        private readonly ProjectPiggyContext _context;
        public DbValues(ProjectPiggyContext context)
        {
            _context = context;
        }

        public void Add(Values obj)
        {
            _context.Values.Add(obj);
        }

        public void Delete(int id)
        {
            var values = new Values { ValuesID = id };
            _context.Values.Attach(values);
            _context.Values.Remove(values);
            _context.SaveChanges();
        }

        public Values Get(int id)
        {
            return _context.Values.Find(id);
        }

        public Values Get(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Values> List()
        {
            return _context.Values;
        }

        public IEnumerable<Values> List(string name)
        {
            throw new NotImplementedException();
        }

        public void Update(Values obj)
        {
            _context.Values.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
