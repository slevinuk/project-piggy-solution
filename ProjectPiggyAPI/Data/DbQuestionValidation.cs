﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI.Data
{
    public class DbQuestionValidation : IFullDataAccess<QuestionValidation>
    {
        private readonly ProjectPiggyContext _context;
        public DbQuestionValidation(ProjectPiggyContext context)
        {
            _context = context;
        }

        public void Add(QuestionValidation obj)
        {
            _context.QuestionValidation.Add(obj);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var questionValidation = new QuestionValidation { QuestionValidationID = id };
            _context.QuestionValidation.Attach(questionValidation);
            _context.QuestionValidation.Remove(questionValidation);
        }

        public QuestionValidation Get(int id)
        {
            return _context.QuestionValidation.Find(id);
        }

        public QuestionValidation Get(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<QuestionValidation> List()
        {
            return _context.QuestionValidation;
        }

        public IEnumerable<QuestionValidation> List(string name)
        {
            throw new NotImplementedException();
        }

        public void Update(QuestionValidation obj)
        {
            _context.QuestionValidation.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
