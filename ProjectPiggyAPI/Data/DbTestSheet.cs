﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI.Data
{
    public class DbTestSheet : IFullDataAccess<TestSheet>
    {
        private readonly ProjectPiggyContext _context;
        public DbTestSheet(ProjectPiggyContext context)
        {
            _context = context;
        }

        public void Add(TestSheet obj)
        {
            _context.TestSheet.Add(obj);
        }

        public void Delete(int id)
        {
            var testSheet = new TestSheet { TestSheetID = id };
            _context.TestSheet.Attach(testSheet);
            _context.TestSheet.Remove(testSheet);
            _context.SaveChanges();
        }

        public TestSheet Get(int id)
        {
            return _context.TestSheet.Find(id);
        }

        public TestSheet Get(string name)
        {
            return _context.TestSheet.Where(x => x.TestSheetTitle == name).FirstOrDefault();
        }

        public IEnumerable<TestSheet> List()
        {
            return _context.TestSheet;
        }

        public IEnumerable<TestSheet> List(string name)
        {
            return _context.TestSheet.Where(x => x.TestSheetTitle == name);
        }

        public void Update(TestSheet obj)
        {
            _context.TestSheet.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
