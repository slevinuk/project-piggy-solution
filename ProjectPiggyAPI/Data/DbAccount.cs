﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI.Data
{
    public class DbAccount : IAccount<Account>
    {
        private readonly ProjectPiggyContext _context;

        public DbAccount(ProjectPiggyContext context)
        {
            _context = context;
        }

        public Account GetByID(int id)
        {
            return _context.Account.Where(x => x.AccountID == id).SingleOrDefault();
        }

        public bool Login(Account obj)
        {
            Account account = _context.Account.Where(x => x.Username == obj.Username && obj.Password == obj.Password)
                .SingleOrDefault();

            return (account != null) ? true : false;
        }

        public void Logout()
        {
            throw new NotImplementedException();
        }

        public void Register(Account obj)
        {
            _context.Account.Add(obj);
            _context.SaveChanges();
        }
    }
}
