﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI.Data
{
    public class DbTypes : IFullDataAccess<Types>
    {
        private readonly ProjectPiggyContext _context;
        public DbTypes(ProjectPiggyContext context)
        {
            _context = context;
        }

        public void Add(Types obj)
        {
            _context.Types.Add(obj);
        }

        public void Delete(int id)
        {
            var types = new Types { TypesID = id };
            _context.Attach(types);
            _context.Types.Remove(types);
            _context.SaveChanges();
        }

        public Types Get(int id)
        {
            return _context.Types.Find(id);
        }

        public Types Get(string name)
        {
            return _context.Types.Where(x => x.TypesTitle == name).FirstOrDefault();
        }

        public IEnumerable<Types> List()
        {
            return _context.Types;
        }

        public IEnumerable<Types> List(string name)
        {
            return _context.Types.Where(x => x.TypesTitle == name);
        }

        public void Update(Types obj)
        {
            _context.Types.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
        }
    }
}
