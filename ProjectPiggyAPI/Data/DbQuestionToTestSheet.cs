﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI.Data
{
    public class DbQuestionToTestSheet : IAddUpdate<QuestionToTestSheet>
    {
        private readonly ProjectPiggyContext _context;
        public DbQuestionToTestSheet(ProjectPiggyContext context)
        {
            _context = context;
        }

        public void Add(QuestionToTestSheet obj)
        {
            _context.QuestionToTestSheet.Add(obj);
            _context.SaveChanges();
        }

        public void Update(QuestionToTestSheet obj)
        {
            _context.QuestionToTestSheet.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
