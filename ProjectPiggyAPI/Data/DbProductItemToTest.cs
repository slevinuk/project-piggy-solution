﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI.Data
{
    public class DbProductItemToTest : IAddUpdate<ProductItemToTest>, IDelete
    {
        private readonly ProjectPiggyContext _context;
        public DbProductItemToTest(ProjectPiggyContext context)
        {
            _context = context;
        }

        public void Add(ProductItemToTest obj)
        {
            _context.ProductItemToTest.Add(obj);
        }

        public void Delete(int id)
        {
            var productItemToTest = new ProductItemToTest { ProductItemToTestID = id };
            _context.ProductItemToTest.Attach(productItemToTest);
            _context.ProductItemToTest.Remove(productItemToTest);
            _context.SaveChanges();
        }

        public void Update(ProductItemToTest obj)
        {
            throw new NotImplementedException();
        }
    }
}
