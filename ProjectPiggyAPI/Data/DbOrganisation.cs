﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPiggyAPI.Data
{
    public class DbOrganisation : IFullDataAccess<Organisation>
    {

        private readonly ProjectPiggyContext _context;
        public DbOrganisation(ProjectPiggyContext context)
        {
            _context = context;
        }

        public void Add(Organisation obj)
        {
            _context.Organisation.Add(obj);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var organisation = new Organisation { OrganisationID = id };
            _context.Organisation.Attach(organisation);
            _context.Organisation.Remove(organisation);
            _context.SaveChanges();
        }

        public Organisation Get(int id)
        {
            return _context.Organisation.Where(x => x.OrganisationID == id).SingleOrDefault();
        }

        public Organisation Get(string name)
        {
            return _context.Organisation.Where(x => x.Name == name).SingleOrDefault();
        }

        public IEnumerable<Organisation> List()
        {
            return _context.Organisation;
        }

        public IEnumerable<Organisation> List(string name)
        {
            return _context.Organisation.Where(x => x.Name == name);
        }

        public void Update(Organisation obj)
        {
            _context.Organisation.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
